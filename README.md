Setting up your app on //appice.io
Sign-up
You need to sign-up and create an account with appice.io.
1.	Visit https://panel.appice.io and click on the Register button
2.	This brings you to the “Sign up” page
3.	Provide your Name, Email address and your chosen password and create your account
4.	Login using your username and password credentials once your account is approved
Setup your App
To start the process of integrating appICE in your app, you will first need to setup your app on the appICE dashboard.
1.	Click on Setup New App in left panel.
 
2.	Provide the link to your mobile app on the Google Play Store as well as Apple store if you have both versions of your app.
Click Next to go to next page.
 
Click Next to go to next page.
3.	Now you have access to the SDK’s for React Native including the instructions for integration. To make it easier, we have also provided the option to provide your developer’s email address so that those instructions can be emailed to them directly.
 
Click Next to go to next page.
4.	Now you are all ready to start receiving data from your mobile app once the developer completes the integration and publishes the app again on the app store. 
 


















